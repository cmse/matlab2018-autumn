%integrate ODE dR/dt = R-aRF
%              dF/dt = -F+bRF
%loop over values of a and b

%set a,b
avalues=[0.05 0.1 0.2];
bvalues=[0.05 0.2];

%set time interval for integration
tspan=[0 200]; 

%set initial condition
y0=[15;10];

%set ODE45 options
OPTIONS = odeset('reltol',1.0e-8,'abstol',1.0e-8); %reduce error tolerances, use help odeset for more info

%initialize counters
acount=0;
bcount=0;

%call ODE45
for a=avalues %loop over a
    acount = acount+1;
    bcount = 0;
   for b=bvalues %loop over b
       disp('a,b=')
       disp([a b])
        bcount = bcount+1;
        [t,y] = ode45(@(t,y) ode_func(t,y,a,b),tspan,y0,OPTIONS);
        %rearrange results
        R = y(:,1); %Rabbits
        F = y(:,2); %Foxes

        %compute and store means and variances
        Rmean(acount,bcount) = mean(R);
        Rvar(acount,bcount) = var(R);
        Fmean(acount,bcount) = mean(F);
        Fvar(acount,bcount) = var(F);
   end
end


%equilibrium populations
Re = 1./bvalues %compare to Rmean
Fe = 1./avalues %compare to Fmean


%***NOTE*** applying mean and var to R and F isn't really appropriate since
%R and F are not stored at equispaced points in time. This can be fixed by setting
%the points in time where ode45 returns the solution:

Nt=1e4;
tvalues = linspace(0,200,Nt); %this is used in the call to ode45
acount=0;
bcount=0;

%call ODE45
for a=avalues %look over a
    acount = acount+1;
    bcount = 0;
   for b=bvalues %loop over b
       disp('a,b=')
       disp([a b])
        bcount = bcount+1;
        [t,y] = ode45(@(t,y) ode_func(t,y,a,b),tvalues,y0,OPTIONS); %tvalues instead of tspan
        %rearrange results
        R = y(:,1); %Rabbits
        F = y(:,2); %Foxes

        %compute and store means and variances
        Rmean2(acount,bcount) = mean(R);
        Rvar2(acount,bcount) = var(R);
        Fmean2(acount,bcount) = mean(F);
        Fvar2(acount,bcount) = var(F);
   end
end


