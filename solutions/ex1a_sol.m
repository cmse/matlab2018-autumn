%Solutions to exercise 1a
%1. Use a concise matlab expression to build the matrix: A=[1 2 3 4 5;
%                                                           7 5 3 1 -1;        
%                                                           4 8 16 32 64]
%
%
row1 = 1:5;
row2 = 7:-2:-1;
row3 = 2.^[2:6];
A = [row1;row2;row3];


%2. Create a new matrix from:
%(a) the four entries in the bottom-right corner of A
B = A(2:3,4:5);
%(b) The elements of a whose column indices are even and whose row indices are odd (e.g. i,j = 1,2)
C = A(1:2:3,2:2:4);

%3. Give a matlab expression that uses only a single matrix multiplication
%with A to obtain:
%(a) the sum of columns 2 and 4 of A
x=[0 1 0 1 0]';
A*x
A(:,2) + A(:,4) %check

%(b) A with the 2nd and 3rd columns swapped
I = eye(5);
I(:,[2 3]) = I(:,[3 2]); %swap 2nd and 3rd columns of identity matrix
A*I


%3. Use rand to construct a 4x4 matrix, B, of random numbers with a uniform
%distribution on [0 1].

B = rand(4);

%extract a 2 x 2 matrix, C, from the 'center' of B

C = B(2:3,2:3);

%what do C^2 and C.^2 compute?

%Ans: C^2 = C*C, C.^2 = C.*C


