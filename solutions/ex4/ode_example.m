%integrate ODE dR/dt = R-aRF
%              dF/dt = -F+bRF
%Exercise 4: add data processing section

%set a,b
a=0.1;
b=0.2;

%set time interval for integration
tspan=[0 10];

%set initial condition, R0 = 15, F0=10
y0=[15;10];

%set ODE45 options
OPTIONS = odeset('reltol',1.0e-8,'abstol',1.0e-8); %reduce error tolerances, use help odeset for more info

%call ODE45
[t,y] = ode45(@(t,y) ode_func(t,y,a,b),tspan,y0,OPTIONS);

%rearrange results
R = y(:,1); %Rabbits
F = y(:,2); %Foxes



%plot results
figure %time series
plot(t,R,'k-')
hold on
plot(t,F,'b--')
legend('Rabbits','Foxes')
xlabel('time')
ylabel('population')

figure %phase space
plot(R,F,'k-')
xlabel('Rabbits')
ylabel('Foxes')


%Exercise 4: Data Processing
%1. Create a new time array, t2, which consists of 1000 points over the
%integration timespan
t2 = linspace(tspan(1),tspan(2),1000);

%2. Interpolate your results for the rabbit and fox populations to t2 using
%spline (or interp1)
R2 = spline(t,R,t2);
F2 = spline(t,F,t2);

%3. Apply diff to this interpolated results, R2, and to t2, and compute an
%estimate for dR2/dt2
dR2 = diff(R2);
dt2 = diff(t2);
dR2dt2 = dR2./dt2; %approximation for dR2/dt2 at t3 where t3 is defined as:

dt2 = t2(2)-t2(1);
t3 = linspace(dt2/2,tspan(2)-dt2/2,999);


%4. Use this estimate along with R2 and F2 to check your computation of the
%governing equation for R

%first interpolate R and F to t3:
R3 = spline(t,R,t3);
F3 = spline(t,F,t3);

%then, assemble RHS of ODE:
RHS = R3 - a*R3.*F3;

%the difference between dR2dt2 and RHS is a crude error estimate:
e = abs(dR2dt2-RHS);
max(e)

figure
plot(t3,dR2dt2,'-')
hold on
plot(t3,RHS,'r--')
xlabel('time')
ylabel('dR/dt')
legend('Approximate derivative computed with diff','RHS of ODE')
title('Ex4, part 4')

%5. Find all values of R within 5% of its mean. Plot these points over a
%curve of R vs t. 
Rmean = mean(R2); %mean of RI
[i] = find(abs(R2-Rmean) < 0.05*Rmean); %finds points with 5% of mean

figure
plot(t2,R2,'b-')
hold on
plot(t2(i),R2(i),'rx')
xlabel('time')
ylabel('Rabbits')
legend('Rabbit population','Points within 5% of mean R population')
title('Ex4, part 5')



















