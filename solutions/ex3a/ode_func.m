%function to calculate RHS of dR/dt = R-aRF
%                             dF/dt = -F+bRF
%here, y(1) is R and y(2) is F
function [dy] = ode_func(t,y,a,b)
    
    R=y(1); %Rabbits
    F=y(2); %Foxes
    
    %RHS's
    dR = R - a*R*F;
    dF = -F + b*R*F;
    
    %values to be returned
    dy = [dR;dF];

return;