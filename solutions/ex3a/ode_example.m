%integrate ODE dR/dt = R-aRF
%              dF/dt = -F+bRF

%set a,b
a=0.1;
b=0.2;

%set time interval for integration
tspan=[0 10];

%set initial condition, R0 = 15, F0=10
y0=[15;10];

%set ODE45 options
OPTIONS = odeset('reltol',1.0e-8,'abstol',1.0e-8); %reduce error tolerances, use help odeset for more info

%call ODE45
[t,y] = ode45(@(t,y) ode_func(t,y,a,b),tspan,y0,OPTIONS);

%rearrange results
R = y(:,1); %Rabbits
F = y(:,2); %Foxes



%plot results
figure %time series
plot(t,R,'k-')
hold on
plot(t,F,'b--')
legend('Rabbits','Foxes')
xlabel('time')
ylabel('population')

figure %phase space
plot(R,F,'k-')
xlabel('Rabbits')
ylabel('Foxes')
