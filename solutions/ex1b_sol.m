%Solutions to exercise 1b
%1. Construct an array, t, consisting of 1000 points fom 0 to 2pi, and
%create arrays corresponding to:
%(a) f=sin(t)
%(b) g=tanh(t)
%(c) h = 1000 random numbers sampled from a normal distribution

t = linspace(0,2*pi,1000);
f = sin(t);
g = tanh(t);
h = randn(1000,1);

%2. Use subplot to make three sub-figures plotting f,g, and h vs. t
figure
subplot(3,1,1), plot(t,f)
subplot(3,1,2), plot(t,g)
subplot(3,1,3), plot(t,h)

%3. Now, plot f, g, and h vs. t on a single plot using a black solid line
%with linewidth = 2, red dashed line, and green dotted line, respectively.

figure
plot(t,f,'k-','linewidth',2)
hold on
plot(t,g,'r--')
plot(t,h,'g:')

%(a) Add a legend to the figure
legend('f','g','h')

%(b) Label the axes and add a title
set(gca,'fontsize',16) %default Matlab fontsize is too small
xlabel('t')
ylabel('f, g, and h')
title('Plotting example')

%(c) Set the x-axis limits to [0 pi]
xlim([0 pi])

%4. Use hist(h) to verivy that h corresponds to a normal distribution
figure
hist(h)

