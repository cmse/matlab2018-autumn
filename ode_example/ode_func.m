%function to calculate RHS of dy/dt=-ay for use with ode45
function [dy] = ode_func(t,y,a)
    dy = -a*y;    
return;