%Create simple animation of a sine wave
nFrames = 20; %number of frames

% Preallocate movie structure.
vidObj = VideoWriter('sine.avi');
open(vidObj);

% Create movie of sin(kx) with k varying
x=linspace(0,2*pi,1000);

%open and 'freeze' figure axes
figure
set(gca,'fontsize',16) %increases fontsize for figure labels, use get(gca)
					   %to see other adjustable properties
for k = 1:nFrames 
    f=sin(k*x); %function to be plotted
    plot(x,f,'k-')
    
    %--add figure labels and set axis limits
	title('animation example')
    label_text = ['k=', num2str(k)]; 
	label_position = [5,0.7];
    text(label_position(1),label_position(2),label_text,'fontsize',16,...
           'fontweight','bold');
    xlabel('x')
    ylabel('sin(kx)')
    axis([0 2*pi -1 1])
    %--
    
    pause(0.1)
    mov = getframe(gcf); %add current figure as frame to mov
    writeVideo(vidObj,mov); %write frame to vidObj
end

% Close the file.
close(vidObj);